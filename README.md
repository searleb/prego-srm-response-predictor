# PREGO Peptide Response Predictor
**Using data independent acquisition to model high-responding peptides for targeted proteomics experiments.**

*Brian C. Searle, Jarrett D. Egertson, James G. Bollinger, Andrew B. Stergachis and Michael J. MacCoss
Published by Molecular and Cellular Proteomics on June 22, 2015, doi: 10.1074/mcp.M115.051300*

PREGO is a software tool that predicts high responding peptides for SRM experiments. PREGO predicts peptide responses with an artificial neural network trained using 11 minimally redundant, maximally relevant properties. Crucial to its success, PREGO is trained using fragment ion intensities of equimolar synthetic peptides extracted from data independent acquisition (DIA) experiments.

Our approach is described in [this talk from ASMS 2015](https://github.com/briansearle/intensity_predictor/blob/master/asms%20talk%202015%20with%20annotations.pdf).
PREGO is offered as [an external tool for Skyline](https://brendanx-uw1.gs.washington.edu/labkey/skyts/home/software/Skyline/tools/details.view?name=Prego). 
PREGO is also offered as [a standalone cross platform tool](https://github.com/briansearle/intensity_predictor/tree/master/installers) with both graphical and command line interfaces.

You can download individual PREGO installers by right clicking on the links below and selecting "Save Link As...":

* [Windows 64-bit](https://github.com/briansearle/intensity_predictor/blob/master/installers/PREGO_windows-x64_0_1_2.exe?raw=true)

* [Windows 32-bit](https://github.com/briansearle/intensity_predictor/blob/master/installers/PREGO_windows_0_1_2.exe?raw=true)

* [Mac 64-bit](https://github.com/briansearle/intensity_predictor/blob/master/installers/PREGO_macos_0_1_2.dmg?raw=true)

* [Linux 64-bit](https://github.com/briansearle/intensity_predictor/blob/master/installers/PREGO_unix_0_1_2.sh?raw=true)