/**
 * Copyright 2015 Brian C. Searle
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.washington.maccoss.intensity_predictor.properties;

import java.io.Serializable;

public class TerminiAAProperty extends AbstractProperty implements Serializable {
	private static final long serialVersionUID=1L;
	private final char aa;
	private final boolean isNTerm;
	private final int numAAFromTerm;
	
	public TerminiAAProperty(char aa, boolean isNTerm, int numAAFromTerm) {
		super(false);
		this.aa=aa;
		this.isNTerm=isNTerm;
		this.numAAFromTerm=numAAFromTerm;
	}

	@Override
	public String toString() {
		if (isNTerm) {
			return aa+" "+numAAFromTerm+" from n-term";
		} else {
			return aa+" "+numAAFromTerm+" from c-term";
		}
	}
	
	@Override
	public double getProperty(String sequence) {
		if (isNTerm) {
			if (sequence.length()<numAAFromTerm) return 0;
			if (sequence.charAt(numAAFromTerm)==aa) return 1;
			return 0;
		} else {
			if (sequence.length()<numAAFromTerm) return 0;
			if (sequence.charAt(sequence.length()-1-numAAFromTerm)==aa) return 1;
			return 0;
		}
	}
}
