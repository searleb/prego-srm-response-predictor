/**
 * Copyright 2015 Brian C. Searle
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.washington.maccoss.intensity_predictor.properties;

import java.io.Serializable;

public class NumberDiAAProperty extends AbstractProperty implements Serializable {
	private static final long serialVersionUID=1L;
	private final char aa1;
	private final char aa2;
	
	public NumberDiAAProperty(char aa1, char aa2) {
		super(false);
		this.aa1=aa1;
		this.aa2=aa2;
	}

	@Override
	public String toString() {
		return "Number of "+aa1+"/"+aa2+" Pairs";
	}
	
	@Override
	public double getProperty(String sequence) {
		int increment=0;
		for (int i=1; i<sequence.length(); i++) {
			if (sequence.charAt(i-1)==aa1&&sequence.charAt(i)==aa2) {
				increment++;
			}
		}
		return increment;
	}
}
