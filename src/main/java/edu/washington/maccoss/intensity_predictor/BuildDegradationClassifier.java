/**
 * Copyright 2015 Brian C. Searle
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.washington.maccoss.intensity_predictor;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.StringTokenizer;

import org.apache.commons.math.stat.inference.TTestImpl;

import edu.washington.maccoss.intensity_predictor.NeuralNetworkGenerator.ScoredArray;
import edu.washington.maccoss.intensity_predictor.math.BackPropNeuralNetwork;
import edu.washington.maccoss.intensity_predictor.math.General;
import edu.washington.maccoss.intensity_predictor.math.NeuralNetworkData;
import edu.washington.maccoss.intensity_predictor.parsers.AAIndex1Parser;
import edu.washington.maccoss.intensity_predictor.properties.AbstractProperty;
import edu.washington.maccoss.intensity_predictor.properties.LengthProperty;
import edu.washington.maccoss.intensity_predictor.properties.MassProperty;
import edu.washington.maccoss.intensity_predictor.properties.NumberAAProperty;
import edu.washington.maccoss.intensity_predictor.properties.NumberAcidicProperty;
import edu.washington.maccoss.intensity_predictor.properties.NumberBasicProperty;
import edu.washington.maccoss.intensity_predictor.properties.NumberDiAAProperty;
import edu.washington.maccoss.intensity_predictor.properties.PropertyInterface;
import edu.washington.maccoss.intensity_predictor.properties.TerminiAAProperty;
import edu.washington.maccoss.intensity_predictor.structures.AbstractPeptide;
import edu.washington.maccoss.intensity_predictor.structures.PeptideWithScores;
import edu.washington.maccoss.intensity_predictor.structures.Protein;
import gnu.trove.list.array.TDoubleArrayList;
import gnu.trove.list.array.TIntArrayList;

public class BuildDegradationClassifier {
	public static final int TOTAL_FEATURES_CONSIDERED=10;
	public static void main(String[] args) {
		args=new String[] {
				"/Users/searleb/Documents/freezer_experiment/neural_network/freezer_peptides.txt",
				"/Users/searleb/Documents/freezer_experiment/neural_network/freezer.nn"
		};
		if (args.length!=2) {
			Logger.writeError("Incorrect number of arguments! BuildClassifier takes two arguments, the intensity file and the location of the new neural network.");
			if (args.length>0) Logger.writeError("The arguments you specified were:");
			for (int i=0; i<args.length; i++) {
				Logger.writeError("\t("+(i+1)+") ["+args[i]+"]");	
			}
			System.exit(1);
		}
		
		if (!args[1].endsWith(".nn")) {
			Logger.writeError("Adding '.nn' extension to the neural network file.");
			args[1]=args[1]+".nn";
		}
		
		File peptidesWithIntensityFile=new File(args[0]);
		File neuralNetworkFile=new File(args[1]);
		
		if (!peptidesWithIntensityFile.exists()||!peptidesWithIntensityFile.canRead()) {
			Logger.writeError("Can't read the intensity file! Please make sure it available and readable. The file you specified was:");
			Logger.writeError("\t["+peptidesWithIntensityFile.getAbsolutePath()+"]");
			System.exit(1);
		}
		
		BackPropNeuralNetwork backprop=buildNN(peptidesWithIntensityFile, TOTAL_FEATURES_CONSIDERED, false, 0.3, false);
		Logger.writeLog("Saving network to "+neuralNetworkFile.getName()+"...");
		NeuralNetworkData.saveNetwork(backprop, neuralNetworkFile);
		Logger.writeLog("Finished!");
	}

	private static BackPropNeuralNetwork buildNN(File peptidesWithIntensityFile, int numFeatures, boolean useSpearmans, double minCorrelationForGrouping, boolean useMRMR) {

		File f=new File(peptidesWithIntensityFile.getParentFile(), "properties.tsv");
		BufferedWriter writer=null;
		try {
			writer=new BufferedWriter(new FileWriter(f));

			Logger.writeLog("Extracting properties from "+peptidesWithIntensityFile.getName()+"...");
			ArrayList<PropertyInterface> properties=getProperties();
			String[] propertyNames=new String[properties.size()];
			writer.write("Sequence\tIntensity");
			for (int i=0; i<propertyNames.length; i++) {
				propertyNames[i]=properties.get(i).toString();
				writer.write("\t"+propertyNames[i]);
			}
			writer.newLine();

			ArrayList<AbstractPeptide> peptides=getPeptides(peptidesWithIntensityFile, properties);

			double[] trainingIntensities=new double[peptides.size()];
			for (int i=0; i<trainingIntensities.length; i++) {
				AbstractPeptide peptide=peptides.get(i);
				trainingIntensities[i]=peptide.getIntensity();
				writer.write(peptide.getSequence()+"\t"+peptide.getIntensity()+"\t"+General.toPropertyString(peptide.getScoreArray()));
				writer.newLine();
			}
			writer.flush();

			double[][] trainingValues=new double[propertyNames.length][];
			TIntArrayList bestFeatureIndicies;
			if (useMRMR) {
				bestFeatureIndicies=NeuralNetworkGenerator.getMRMRFeatureIndicies(peptides, trainingIntensities, trainingValues, propertyNames, numFeatures, useSpearmans, minCorrelationForGrouping);
			} else {
				bestFeatureIndicies=NeuralNetworkGenerator.getBestFeatureIndicies(peptides, trainingIntensities, trainingValues, propertyNames, numFeatures, false, useSpearmans);
			}
			ArrayList<AbstractProperty> usedProperties=new ArrayList<AbstractProperty>();
			for (int index : bestFeatureIndicies.toArray()) {
				usedProperties.add((AbstractProperty)properties.get(index));
			}

			Logger.writeLog("Building network...");

			FeatureSet trainingSet=getFeatureSet(peptides, trainingIntensities, trainingValues, bestFeatureIndicies);
			
			System.out.println("Original data size: "+trainingSet.size());
			trainingSet=trainingSet.getTrimmedSet();
			
			System.out.println("Trimmed to match good and bad: "+trainingSet.size());
			FeatureSet testSet=trainingSet.removeTestSet(0.2);
			
			System.out.println("Training: "+trainingSet.size()+", Testing: "+testSet.size());
			BackPropNeuralNetwork backprop=getNeuralNetwork(trainingSet, usedProperties);
			
			System.exit(1);
			for (int i=0; i<testSet.goodPeptides.size(); i++) {
				double score=backprop.getScore(testSet.goodPeptides.get(i).getSequence());
				System.out.println(testSet.goodPeptides.get(i).getSequence()+"\t"+testSet.goodIntensities.get(i)+"\t"+score);
			}
			for (int i=0; i<testSet.badPeptides.size(); i++) {
				double score=backprop.getScore(testSet.badPeptides.get(i).getSequence());
				System.out.println(testSet.badPeptides.get(i).getSequence()+"\t"+testSet.badIntensities.get(i)+"\t"+score);
			}

			return backprop;
		} catch (IOException ioe) {
			return null;
		} finally {
			if (writer!=null) {
				try {
					writer.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static TIntArrayList getBestFeatureIndicies(FeatureSet set, String[] scoreNames, int numberOfFeatures) {
		TTestImpl test=new TTestImpl();
		ArrayList<ScoredArray> features=new ArrayList<ScoredArray>();
		for (int i=0; i<scoreNames.length; i++) {
			TDoubleArrayList goodScores=new TDoubleArrayList();
			for (int j=0; j<set.goodPeptides.size(); j++) {
				double score=set.goodFeatures.get(j)[i];
				goodScores.add(score);
			}
			TDoubleArrayList badScores=new TDoubleArrayList();
			for (int j=0; j<set.badPeptides.size(); j++) {
				double score=set.badFeatures.get(j)[i];
				goodScores.add(score);
			}
			
			TDoubleArrayList allScores=new TDoubleArrayList(goodScores);
			allScores.addAll(badScores);
			double tStatistic=test.t(goodScores.toArray(), badScores.toArray());
			features.add(new ScoredArray(tStatistic, allScores.toArray(), i, scoreNames[i]));
		}

		Collections.sort(features);
		
		TIntArrayList bestFeatureIndicies=new TIntArrayList();
		for (int iter=0; iter<numberOfFeatures; iter++) {
			ScoredArray best=features.remove(features.size()-1);
			bestFeatureIndicies.add(best.getIndex());
			Logger.writeLog("T-Statistic Feature: ("+bestFeatureIndicies.size()+") Statistic: "+best.toString());
		}

		return bestFeatureIndicies;
	}
	
	static class FeatureSet {
		ArrayList<AbstractPeptide> goodPeptides;
		ArrayList<double[]> goodFeatures;
		TDoubleArrayList goodIntensities;
		ArrayList<AbstractPeptide> badPeptides;
		ArrayList<double[]> badFeatures;
		TDoubleArrayList badIntensities;
		public FeatureSet(ArrayList<AbstractPeptide> goodPeptides, ArrayList<double[]> goodFeatures, TDoubleArrayList goodIntensities, ArrayList<AbstractPeptide> badPeptides, ArrayList<double[]> badFeatures, TDoubleArrayList badIntensities) {
			this.goodPeptides=goodPeptides;
			this.goodFeatures=goodFeatures;
			this.goodIntensities=goodIntensities;
			this.badPeptides=badPeptides;
			this.badFeatures=badFeatures;
			this.badIntensities=badIntensities;
			assert(goodFeatures.size()==badFeatures.size());
			assert(goodFeatures.size()==goodIntensities.size());
			assert(badFeatures.size()==badIntensities.size());
		}
		
		public int size() {
			return goodPeptides.size()+badPeptides.size();
		}
		
		public FeatureSet mergeWith(FeatureSet s) {
			ArrayList<AbstractPeptide> goodPeptidesTest=new ArrayList<>(goodPeptides);
			ArrayList<double[]> goodFeaturesTest=new ArrayList<>(goodFeatures);
			TDoubleArrayList goodIntensitiesTest=new TDoubleArrayList(goodIntensities);
			ArrayList<AbstractPeptide> badPeptidesTest=new ArrayList<>(badPeptides);
			ArrayList<double[]> badFeaturesTest=new ArrayList<>(badFeatures);
			TDoubleArrayList badIntensitiesTest=new TDoubleArrayList(badIntensities);
			
			goodPeptidesTest.addAll(s.goodPeptides);
			goodFeaturesTest.addAll(s.goodFeatures);
			goodIntensitiesTest.addAll(s.goodIntensities);
			badPeptidesTest.addAll(s.badPeptides);
			badFeaturesTest.addAll(s.badFeatures);
			badIntensitiesTest.addAll(s.badIntensities);

			return new FeatureSet(goodPeptidesTest, goodFeaturesTest, goodIntensitiesTest, badPeptidesTest, badFeaturesTest, badIntensitiesTest);
		}
		
		public FeatureSet removeTestSet(double percentage) {
			int targetSize=(int)Math.round(goodFeatures.size()*(1.0-percentage));

			ArrayList<AbstractPeptide> goodPeptidesTest=new ArrayList<>();
			ArrayList<double[]> goodFeaturesTest=new ArrayList<>();
			TDoubleArrayList goodIntensitiesTest=new TDoubleArrayList();
			ArrayList<AbstractPeptide> badPeptidesTest=new ArrayList<>();
			ArrayList<double[]> badFeaturesTest=new ArrayList<>();
			TDoubleArrayList badIntensitiesTest=new TDoubleArrayList();
			// remove random entries to ensure sizes are equivalent
			while (goodIntensities.size()>targetSize) {
				int index=(int)Math.floor(Math.random()*goodIntensities.size());
				badPeptidesTest.add(badPeptides.remove(index));
				badIntensitiesTest.add(badIntensities.removeAt(index));
				badFeaturesTest.add(badFeatures.remove(index));

				goodPeptidesTest.add(goodPeptides.remove(index));
				goodIntensitiesTest.add(goodIntensities.removeAt(index));
				goodFeaturesTest.add(goodFeatures.remove(index));
			}
			return new FeatureSet(goodPeptidesTest, goodFeaturesTest, goodIntensitiesTest, badPeptidesTest, badFeaturesTest, badIntensitiesTest);
		}
		
		public FeatureSet removeBasedOnPositiveProperty(AbstractProperty property) {
			ArrayList<AbstractPeptide> goodPeptidesTest=new ArrayList<>();
			ArrayList<double[]> goodFeaturesTest=new ArrayList<>();
			TDoubleArrayList goodIntensitiesTest=new TDoubleArrayList();
			ArrayList<AbstractPeptide> badPeptidesTest=new ArrayList<>();
			ArrayList<double[]> badFeaturesTest=new ArrayList<>();
			TDoubleArrayList badIntensitiesTest=new TDoubleArrayList();
			
			int index=0;
			while (index<goodPeptides.size()) {
				if (property.getProperty(goodPeptides.get(index).getSequence())>0) {
					goodPeptidesTest.add(goodPeptides.remove(index));
					goodIntensitiesTest.add(goodIntensities.removeAt(index));
					goodFeaturesTest.add(goodFeatures.remove(index));
				} else {
					index++;
				}
			}
			index=0;
			while (index<badPeptides.size()) {
				if (property.getProperty(badPeptides.get(index).getSequence())>0) {
					badPeptidesTest.add(badPeptides.remove(index));
					badIntensitiesTest.add(badIntensities.removeAt(index));
					badFeaturesTest.add(badFeatures.remove(index));
				} else {
					index++;
				}
			}
			return new FeatureSet(goodPeptidesTest, goodFeaturesTest, goodIntensitiesTest, badPeptidesTest, badFeaturesTest, badIntensitiesTest);
		}
		
		public FeatureSet getTrimmedSet()  {
			ArrayList<AbstractPeptide> goodPeptidesTest=new ArrayList<>(goodPeptides);
			ArrayList<double[]> goodFeaturesTest=new ArrayList<>(goodFeatures);
			TDoubleArrayList goodIntensitiesTest=new TDoubleArrayList(goodIntensities);
			ArrayList<AbstractPeptide> badPeptidesTest=new ArrayList<>(badPeptides);
			ArrayList<double[]> badFeaturesTest=new ArrayList<>(badFeatures);
			TDoubleArrayList badIntensitiesTest=new TDoubleArrayList(badIntensities);

			// remove random entries to ensure sizes are equivalent
			while (goodIntensitiesTest.size()<badIntensitiesTest.size()) {
				int index=(int)Math.floor(Math.random()*badIntensitiesTest.size());
				badPeptidesTest.remove(index);
				badIntensitiesTest.removeAt(index);
				badFeaturesTest.remove(index);
			}
			
			while (goodIntensitiesTest.size()>badIntensitiesTest.size()) {
				int index=(int)Math.floor(Math.random()*goodIntensitiesTest.size());
				goodPeptidesTest.remove(index);
				goodIntensitiesTest.removeAt(index);
				goodFeaturesTest.remove(index);
			}
			
			return new FeatureSet(goodPeptidesTest, goodFeaturesTest, goodIntensitiesTest, badPeptidesTest, badFeaturesTest, badIntensitiesTest);
		}
	}

	public static FeatureSet getFeatureSet(ArrayList<AbstractPeptide> peptides, double[] originalIntensities, double[][] values, TIntArrayList bestFeatureIndicies) {
		ArrayList<double[]> bestFeatures=new ArrayList<double[]>();
		for (int index : bestFeatureIndicies.toArray()) {
			bestFeatures.add(values[index]);
		}
		
		ArrayList<AbstractPeptide> goodPeptides=new ArrayList<>();
		ArrayList<AbstractPeptide> badPeptides=new ArrayList<>();
		ArrayList<double[]> goodFeatures=new ArrayList<double[]>();
		ArrayList<double[]> badFeatures=new ArrayList<double[]>();
		TDoubleArrayList goodIntensities=new TDoubleArrayList();
		TDoubleArrayList badIntensities=new TDoubleArrayList();
		for (int i=0; i<originalIntensities.length; i++) {
			double[] featureArray=new double[bestFeatures.size()];
			for (int j=0; j<bestFeatures.size(); j++) {
				featureArray[j]=bestFeatures.get(j)[i];
			}
			if (0.5>originalIntensities[i]) {
				badPeptides.add(peptides.get(i));
				badFeatures.add(featureArray);
				badIntensities.add(0.0);
			} else if (0.5<originalIntensities[i]) {
				goodPeptides.add(peptides.get(i));
				goodFeatures.add(featureArray);
				goodIntensities.add(1.0);
			}
		}
		System.out.println("Pre-data set sizes: "+goodPeptides.size()+", "+goodFeatures.size()+", "+goodIntensities.size()+", "+
				badPeptides.size()+", "+badFeatures.size()+", "+badIntensities.size());
		FeatureSet set=new FeatureSet(goodPeptides, goodFeatures, goodIntensities, badPeptides, badFeatures, badIntensities);
		return set;
	}

	public static BackPropNeuralNetwork getNeuralNetwork(FeatureSet set, ArrayList<AbstractProperty> finalPropertyList) {
		double[][] goodData=set.goodFeatures.toArray(new double[set.goodFeatures.size()][]);
		double[][] badData=set.badFeatures.toArray(new double[set.badFeatures.size()][]);
		BackPropNeuralNetwork backprop=BackPropNeuralNetwork.buildModel(goodData, set.goodIntensities.toArray(), badData, set.badIntensities.toArray(), finalPropertyList);
		return backprop;
	}

	private static ArrayList<AbstractPeptide> getPeptides(File peptidesWithIntensityFile, ArrayList<PropertyInterface> properties) {
		ArrayList<AbstractPeptide> peptides=new ArrayList<AbstractPeptide>();
		Protein protein=new Protein(peptidesWithIntensityFile.getName());
		BufferedReader reader=null;
		try {
			reader=new BufferedReader(new FileReader(peptidesWithIntensityFile));
			String line=null;
			while ((line=reader.readLine())!=null) {
				if (line.startsWith("#")) continue; // comment
				StringTokenizer st=new StringTokenizer(line);
				String sequence=st.nextToken();
				float intensity=0.0f;
				if (st.hasMoreTokens()) {
					intensity=Float.parseFloat(st.nextToken());
				}
				TDoubleArrayList peptideProperties=new TDoubleArrayList();
				for (PropertyInterface property : properties) {
					peptideProperties.add(property.getProperty(sequence));
				}
				PeptideWithScores peptide=new PeptideWithScores(sequence, intensity, protein, peptideProperties.toArray());
				peptides.add(peptide);
			}

		} catch (IOException ioe) {
			ioe.printStackTrace();
		} finally {
			try {
				if (reader!=null) reader.close();
			} catch (IOException ioe) {
				ioe.printStackTrace();
			}
		}
		return peptides;
	}

	private static ArrayList<PropertyInterface> getProperties() {
		ArrayList<PropertyInterface> properties=new ArrayList<PropertyInterface>();
		properties.add(new LengthProperty());
		properties.add(new NumberBasicProperty());
		properties.add(new NumberAcidicProperty());
		properties.add(new MassProperty());
		
		for (char c : "ACDEFGHIKLMNPQRSTVWY".toCharArray()) {
			properties.add(new NumberAAProperty(c));
			properties.add(new TerminiAAProperty(c, true, 0));
			properties.add(new TerminiAAProperty(c, true, 1));
			properties.add(new TerminiAAProperty(c, false, 0));
			properties.add(new TerminiAAProperty(c, false, 1));
			for (char c2 : "ACDEFGHIKLMNPQRSTVWY".toCharArray()) {
				properties.add(new NumberDiAAProperty(c, c2));
			}
		}
		
		try {
			URI uri=AAIndex1Parser.class.getClassLoader().getResource("aaindex1").toURI();
			File f=new File(uri);
			properties.addAll(AAIndex1Parser.parseAAIndex1(f, false));
		} catch (URISyntaxException urise) {
			Logger.writeError("Error parsing amino acid properties file.");
			Logger.writeError(urise);
			fail();
		}
		return properties;
	}
	
	private static void fail() {
		System.exit(1);
	}
}
